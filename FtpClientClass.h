/*----- PROTECTED REGION ID(FtpClientClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        FtpClientClass.h
//
// description : Include for the FtpClient root class.
//               This class is the singleton class for
//                the FtpClient device class.
//               It contains all properties and methods which the 
//               FtpClient requires only once e.g. the commands.
//
// project :     FtpClient
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef FtpClientClass_H
#define FtpClientClass_H

#include <tango.h>
#include <FtpClient.h>


/*----- PROTECTED REGION END -----*/	//	FtpClientClass.h


namespace FtpClient_ns
{
/*----- PROTECTED REGION ID(FtpClientClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	FtpClientClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute FtpHostName class definition
class FtpHostNameAttrib: public Tango::Attr
{
public:
	FtpHostNameAttrib():Attr("FtpHostName",
			Tango::DEV_STRING, Tango::READ_WRITE) {};
	~FtpHostNameAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<FtpClient *>(dev))->read_FtpHostName(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<FtpClient *>(dev))->write_FtpHostName(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<FtpClient *>(dev))->is_FtpHostName_allowed(ty);}
};

//	Attribute FtpUser class definition
class FtpUserAttrib: public Tango::Attr
{
public:
	FtpUserAttrib():Attr("FtpUser",
			Tango::DEV_STRING, Tango::READ) {};
	~FtpUserAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<FtpClient *>(dev))->read_FtpUser(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<FtpClient *>(dev))->is_FtpUser_allowed(ty);}
};

//	Attribute Connected class definition
class ConnectedAttrib: public Tango::Attr
{
public:
	ConnectedAttrib():Attr("Connected",
			Tango::DEV_BOOLEAN, Tango::READ) {};
	~ConnectedAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<FtpClient *>(dev))->read_Connected(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<FtpClient *>(dev))->is_Connected_allowed(ty);}
};

//	Attribute CurrentDir class definition
class CurrentDirAttrib: public Tango::Attr
{
public:
	CurrentDirAttrib():Attr("CurrentDir",
			Tango::DEV_STRING, Tango::READ_WRITE) {};
	~CurrentDirAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<FtpClient *>(dev))->read_CurrentDir(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<FtpClient *>(dev))->write_CurrentDir(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<FtpClient *>(dev))->is_CurrentDir_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command Connect class definition
class ConnectClass : public Tango::Command
{
public:
	ConnectClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ConnectClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ConnectClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_Connect_allowed(any);}
};

//	Command Disconnect class definition
class DisconnectClass : public Tango::Command
{
public:
	DisconnectClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	DisconnectClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~DisconnectClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_Disconnect_allowed(any);}
};

//	Command Dir class definition
class DirClass : public Tango::Command
{
public:
	DirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	DirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~DirClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_Dir_allowed(any);}
};

//	Command mkdir class definition
class mkdirClass : public Tango::Command
{
public:
	mkdirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	mkdirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~mkdirClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_mkdir_allowed(any);}
};

//	Command rmdir class definition
class rmdirClass : public Tango::Command
{
public:
	rmdirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	rmdirClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~rmdirClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_rmdir_allowed(any);}
};

//	Command PutFile class definition
class PutFileClass : public Tango::Command
{
public:
	PutFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	PutFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~PutFileClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_PutFile_allowed(any);}
};

//	Command GetFile class definition
class GetFileClass : public Tango::Command
{
public:
	GetFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	GetFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~GetFileClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_GetFile_allowed(any);}
};

//	Command RmFile class definition
class RmFileClass : public Tango::Command
{
public:
	RmFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	RmFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~RmFileClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_RmFile_allowed(any);}
};

//	Command WriteFile class definition
class WriteFileClass : public Tango::Command
{
public:
	WriteFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	WriteFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~WriteFileClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_WriteFile_allowed(any);}
};

//	Command ReadFile class definition
class ReadFileClass : public Tango::Command
{
public:
	ReadFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ReadFileClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ReadFileClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_ReadFile_allowed(any);}
};

//	Command ReadSize class definition
class ReadSizeClass : public Tango::Command
{
public:
	ReadSizeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	ReadSizeClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~ReadSizeClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<FtpClient *>(dev))->is_ReadSize_allowed(any);}
};


/**
 *	The FtpClientClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  FtpClientClass : public Tango::DeviceClass
#else
class FtpClientClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(FtpClientClass::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	FtpClientClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static FtpClientClass *init(const char *);
		static FtpClientClass *instance();
		~FtpClientClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		FtpClientClass(string &);
		static FtpClientClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	FtpClient_H
